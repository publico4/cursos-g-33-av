import React, { useEffect } from "react";
import Layout from "../../Layout";
import Router, { useRouter } from "next/router";
import firebase from "../../firebase";
import { formatDistanceToNow } from "date-fns";
import { es } from "date-fns/locale";
import { useCurso } from "../../hooks/useCurso";

const Curso = ({ curso, existeCurso }) => {
  const router = useRouter();

  const {
    query: { id },
  } = router;

  // useEffect(() => {
  //   if (!existeCurso) {
  //     return Router.push("/");
  //   }
  // }, []);

  const {
    miCurso,
    usuario,
    mensaje,
    loaderComentario,
    loaderMeGusta,
    loaderEliminar,
    comentarioChange,
    agregarComentario,
    votarCurso,
    eleminarCurso,
  } = useCurso(id, curso);

  const puedoEliminarCurso = () => {
    if (!usuario) return false;
    if (creador.id === usuario.uid) return true;
  };

  const {
    nombre,
    creado,
    urlImage,
    url,
    votos,
    empresa,
    descripcion,
    comentarios,
    creador,
  } = miCurso;

  return (
    <>
      {existeCurso ? (
        <Layout textTitle={`Curso con id ${id}`} urlImage={urlImage}>
          <section className="row">
            <div className="col-md-12 mt-4">
              <h4 className="text-center">{nombre}</h4>
            </div>
            <div className="col-md-6">
              <small className="text-muted">
                Publicado hace{" "}
                {formatDistanceToNow(new Date(creado), { locale: es })}
              </small>
              <img src={urlImage} className="img-fluid" alt={nombre} />
              {console.log("tmr", usuario)}
              {usuario && (
                <>
                  <h4 className="mt-2">Agregar comentario</h4>
                  <form className="row" onSubmit={agregarComentario}>
                    <div className="col-9">
                      <input
                        type="text"
                        className="form-control"
                        name="mensaje"
                        value={mensaje}
                        onChange={comentarioChange}
                      />
                    </div>
                    <div className="col-3">
                      <button
                        type="submit"
                        className="btn btn-primary btn-block"
                      >
                        Agregar{" "}
                        {loaderComentario && (
                          <span
                            className="spinner-border spinner-border-sm"
                            role="status"
                            aria-hidden="true"
                          ></span>
                        )}
                      </button>
                    </div>
                  </form>
                </>
              )}

              <h4 className="mt-3">Comentarios</h4>
              {comentarios.length === 0 ? (
                <p>Se el primero en dejar tu comentario</p>
              ) : (
                <ul className="list-group">
                  {comentarios.map((comentario, index) => (
                    <li key={index} className="list-group-item">
                      {comentario.mensaje}
                    </li>
                  ))}
                </ul>
              )}
            </div>
            <div className="col-md-6">
              <div className="row">
                <div className="col-12">
                  <div className="mt-4">
                    <a
                      href={url}
                      target="_blank"
                      rel="noreferrer"
                      type="button"
                      className="btn btn-info"
                    >
                      <i
                        className="fa fa-long-arrow-right"
                        aria-hidden="true"
                      ></i>{" "}
                      ir a la página oficial
                    </a>
                  </div>
                  <div className="mt-3">
                    {usuario && (
                      <button
                        type="button"
                        className="btn btn-danger mr-2"
                        onClick={votarCurso}
                      >
                        Me gusta
                        <i className="fa fa-heart-o" aria-hidden="true"></i>
                      </button>
                    )}

                    {loaderMeGusta ? (
                      <div className="spinner-border" role="status">
                        <span className="visually-hidden">Loading...</span>
                      </div>
                    ) : (
                      <span className="badge bg-primary">{votos}</span>
                    )}
                  </div>
                  <div></div>
                </div>
                <div className="col-12 mt-2">
                  <h4>
                    Descripción
                    <small className="text-muted">(empresa: {empresa})</small>
                  </h4>
                  <small className="text-muted">
                    (publicado por : eduardo)
                  </small>
                  <p>{descripcion}</p>
                </div>
                <div className="col-md-12">
                  {puedoEliminarCurso() && (
                    <button
                      className="btn btn-danger btn-block"
                      onClick={eleminarCurso}
                    >
                      Eliminar Producto{" "}
                      {loaderEliminar && (
                        <span
                          className="spinner-border spinner-border-sm"
                          role="status"
                          aria-hidden="true"
                        ></span>
                      )}
                    </button>
                  )}
                </div>
              </div>
            </div>
          </section>
        </Layout>
      ) : (
        <p>No existe el curso</p>
      )}
    </>
  );
};

export default Curso;

export async function getServerSideProps(context) {
  const {
    params: { id },
  } = context;

  const doc = await firebase.db.collection("cursos").doc(id);

  const curso = await doc.get();

  return {
    props: {
      curso: curso.data(),
      existeCurso: true, // true o false
    },
  };
}
