import "bootstrap/dist/css/bootstrap.css";
import firebase, { FirebaseContext } from "../firebase";
import AuthProvider from "../context/auth";
import CursosProvider from "../context/cursos/cursosProvider";

function MyApp({ Component, pageProps }) {
  return (
    <FirebaseContext.Provider value={{ firebase }}>
      <AuthProvider>
        <CursosProvider>
          <Component {...pageProps} />
        </CursosProvider>
      </AuthProvider>
    </FirebaseContext.Provider>
  );
}

export default MyApp;
