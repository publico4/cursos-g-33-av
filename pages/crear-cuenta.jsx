import React, { useContext } from "react";
import Router from "next/router";
import { AuthContext } from "../context/auth";
import Layout from "../Layout";
import { useValidation } from "../hooks/useValidation";
import { validarCrearCuenta } from "../utils/validaciones";
import ErrorInput from "../components/ErrorInput";

const initialState = {
  nombre: "",
  email: "",
  password: "",
};

const CrearCuenta = () => {
  const { valores, errores, handleChange, handleSubmit } = useValidation(
    initialState,
    validarCrearCuenta,
    () => crearCuenta()
  );

  const { crear } = useContext(AuthContext);

  const crearCuenta = () => {
    console.log("SE EJECUTO LA PETICIÓN EXTERNA DE CREAR CUENTA");
    const { nombre, email, password } = valores;
    crear(nombre, email, password);
    Router.push("/");
  };

  return (
    <Layout textTitle="crear cuenta">
      <h1 className="text-center">crear cuenta</h1>
      <form
        noValidate={true}
        onSubmit={handleSubmit}
        style={{ maxWidth: "30em", margin: "0px auto" }}
      >
        <div className="mb-3">
          <label htmlFor="nombre" className="form-label">
            Nombre
          </label>
          <input
            type="text"
            className="form-control"
            name="nombre"
            onChange={handleChange}
            value={valores.nombre}
          />
          {errores.nombre && <ErrorInput text={errores.nombre} />}
        </div>
        <div className="mb-3">
          <label htmlFor="email" className="form-label">
            Correo
          </label>
          <input
            type="email"
            className="form-control"
            name="email"
            onChange={handleChange}
            value={valores.email}
          />
          {errores.email && <ErrorInput text={errores.email} />}
        </div>
        <div className="mb-3">
          <label htmlFor="password" className="form-label">
            Contraseña
          </label>
          <input
            type="password"
            className="form-control"
            name="password"
            onChange={handleChange}
            value={valores.password}
          />
          {errores.password && <ErrorInput text={errores.password} />}
        </div>
        <div className="mb-3">
          <button type="submit" className="btn btn-primary">
            crear cuenta
          </button>
        </div>
      </form>
    </Layout>
  );
};

export default CrearCuenta;
