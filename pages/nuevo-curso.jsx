import React, { useContext, useState } from "react";
import Router from "next/router";
import { FirebaseContext } from "../firebase";
import { AuthContext } from "../context/auth";
import { CursosContext } from "../context/cursos/cursosContext";
import Layout from "../Layout";
import { useValidation } from "../hooks/useValidation";
import { validarCrearCurso } from "../utils/validaciones";
import ErrorInput from "../components/ErrorInput";
const initialState = {
  nombre: "",
  empresa: "",
  imagen: "",
  descripcion: "",
};

const NuevoCUrso = () => {
  const [nombreImage, setNombreImage] = useState("");
  const [urlImage, seturlImage] = useState("");
  const [loadingUpImage, setLoadingUpImage] = useState(null);

  const { valores, errores, handleChange, handleSubmit } = useValidation(
    initialState,
    validarCrearCurso,
    () => nuevoCurso()
  );

  const { firebase } = useContext(FirebaseContext);
  const { state } = useContext(AuthContext);
  const { crearCursoContext } = useContext(CursosContext);

  const handleUploadImage = (e) => {
    const image = e.target.files[0];
    console.log("image", image);

    const uploadTask = firebase.storage.ref(`/cursos/${image.name}`).put(image);

    setLoadingUpImage(true);

    uploadTask.on(
      "state_changed",
      (snapShot) => {
        console.log("snapShot");
        console.log(snapShot);
      },
      (error) => {
        console.log("error");
        console.log(error);
      },
      (event) => {
        firebase.storage
          .ref("cursos")
          .child(image.name)
          .getDownloadURL()
          .then((imagenUrl) => {
            console.log("la url de la imagen es", imagenUrl);
            setLoadingUpImage(false);
            setNombreImage(image.name);
            seturlImage(imagenUrl);
          });
      }
    );
  };

  const nuevoCurso = async () => {
    console.log("SE EJECUTO LA PETICIÓN EXTERNA DE NUEVO CURSO");

    const { nombre, empresa, url, descripcion } = valores;
    const curso = {
      nombre,
      empresa,
      url,
      descripcion,
      nombreImage,
      urlImage,
      votos: 0,
      votantes: [],
      comentarios: [],
      creado: Date.now(),
      creador: {
        id: state.userInfo.uid,
        nombre: state.userInfo.displayName,
      },
    };
    await crearCursoContext(curso);
    Router.push("/");
  };

  return (
    <Layout textTitle="crear nuevo curso">
      <h1 className="text-center my-4">crear nuevo curso</h1>
      <form
        noValidate={true}
        onSubmit={handleSubmit}
        className="row g-3 p-5 bg-light border rounded-3"
      >
        <div className="col-md-6">
          <label htmlFor="nombre" className="form-label">
            Nombre
          </label>
          <input
            type="text"
            className="form-control"
            name="nombre"
            onChange={handleChange}
            value={valores.nombre}
          />
          {errores.nombre && <ErrorInput text={errores.nombre} />}
        </div>
        <div className="col-md-6">
          <label htmlFor="empresa" className="form-label">
            Empresa
          </label>
          <input
            type="text"
            className="form-control"
            name="empresa"
            onChange={handleChange}
            value={valores.empresa}
          />
          {errores.empresa && <ErrorInput text={errores.empresa} />}
        </div>
        <div className="col-6">
          <label htmlFor="url" className="form-label">
            Url
          </label>
          <input
            type="text"
            className="form-control"
            name="url"
            onChange={handleChange}
            value={valores.url}
          />
          {errores.url && <ErrorInput text={errores.url} />}
        </div>
        <div className="col-6">
          <label htmlFor="formFile" className="form-label">
            Cargar imagen
          </label>
          <input
            className="form-control"
            type="file"
            id="formFile"
            onChange={handleUploadImage}
          />
          {loadingUpImage && (
            <div className="spinner-border" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          )}
        </div>
        <div className="col-12">
          <label htmlFor="floatingTextarea2" className="form-label">
            Descripción del curso
          </label>
          <textarea
            className="form-control"
            id="floatingTextarea2"
            style={{ height: "100px" }}
            onChange={handleChange}
            name="descripcion"
            value={valores.descripcion}
          ></textarea>
          {errores.descripcion && <ErrorInput text={errores.descripcion} />}
        </div>
        <div className="col-12">
          <button type="submit" className="btn btn-primary">
            crear curso
          </button>
        </div>
      </form>
    </Layout>
  );
};

export default NuevoCUrso;
