import { useEffect } from "react";
import CardCurso from "../components/CardCurso";
import Layout from "../Layout";
// import { CursosContext } from "../context/cursos/cursosContext";

import firebase from "../firebase";

export default function Home({ saludo, cursos }) {
  // const { listarCursosContext, cursos } = useContext(CursosContext);

  // useEffect(() => {
  //   listarCursosContext();
  // }, []);

  return (
    <Layout textTitle="Pagina de inicio">
      <div className="row mt-4">
        <h1>{saludo}</h1>
        {cursos &&
          cursos.map((curso) => <CardCurso key={curso.id} curso={curso} />)}
      </div>
    </Layout>
  );
}

export async function getStaticProps() {
  const doc = await firebase.db.collection("cursos").get();
  const cursos = [];

  doc.forEach((curso) => cursos.push({ id: curso.id, ...curso.data() }));

  return {
    props: {
      saludo: "Hola Mundo",
      cursos,
    },
  };
}
