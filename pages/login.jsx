import React, { useContext } from "react";
import { useRouter } from "next/router";
import { AuthContext } from "../context/auth";
import { useValidation } from "../hooks/useValidation";
import { validarLogin } from "../utils/validaciones";
import Layout from "../Layout";
import ErrorInput from "../components/ErrorInput";

const initialState = {
  email: "",
  password: "",
};

const Login = () => {
  const { valores, errores, handleChange, handleSubmit } = useValidation(
    initialState,
    validarLogin,
    () => iniciarSesion()
  );
  const { iniciar } = useContext(AuthContext);
  const router = useRouter();

  const iniciarSesion = async () => {
    const { email, password } = valores;
    await iniciar(email, password);
    router.push("/");
  };

  return (
    <Layout textTitle="iniciar sesión">
      <h1 className="text-center">Iniciar Sesión</h1>
      <form
        noValidate={true}
        onSubmit={handleSubmit}
        style={{ maxWidth: "30em", margin: "0px auto" }}
      >
        <div className="mb-3">
          <label htmlFor="email" className="form-label">
            Correo
          </label>
          <input
            type="email"
            className="form-control"
            name="email"
            onChange={handleChange}
            value={valores.email}
          />
          {errores.email && <ErrorInput text={errores.email} />}
        </div>
        <div className="mb-3">
          <label htmlFor="password" className="form-label">
            Contraseña
          </label>
          <input
            type="password"
            className="form-control"
            name="password"
            onChange={handleChange}
            value={valores.password}
          />
          {errores.password && <ErrorInput text={errores.password} />}
        </div>
        <div className="mb-3">
          <button type="submit" className="btn btn-primary">
            iniciar
          </button>
        </div>
      </form>
    </Layout>
  );
};

export default Login;
