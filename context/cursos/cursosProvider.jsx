import React, { useReducer } from "react";
import { CursosContext } from "./cursosContext";
import reducer from "./cursosReducer";
import firebase from "../../firebase";

const initialState = {
  addCursoOk: false,
  loadingAddCurso: false,
  errorAddCurso: null,
  /**Lista de cursos */
  cursos: null,
};

const CursosProvider = ({ children }) => {
  const crearCursoContext = async (curso) => {
    console.log("en el contexto el curso es", curso);
    dispatch({
      type: "LOADING_ADD_CURSO",
    });

    try {
      await firebase.db.collection("cursos").add(curso);
      dispatch({
        type: "ADD_CURSO_OK",
      });
    } catch (error) {
      dispatch({
        type: "ADD_CURSO_ERROR",
      });
    }
  };

  const listarCursosContext = async () => {
    try {
      const doc = await firebase.db.collection("cursos").get();
      const cursos = [];

      doc.forEach((curso) => cursos.push({ id: curso.id, ...curso.data() }));

      dispatch({
        type: "LISTADO",
        payload: cursos,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const [state, dispatch] = useReducer(reducer, initialState);
  const value = {
    cursos: state.cursos,
    addCursoOk: state.addCursoOk,
    crearCursoContext,
    listarCursosContext
  };

  return (
    <CursosContext.Provider value={value}>{children}</CursosContext.Provider>
  );
};

export default CursosProvider;
