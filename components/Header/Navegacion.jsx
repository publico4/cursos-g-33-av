import React, { useState, useEffect } from "react";
import Link from "next/link";
import Router from "next/router";

const Navegacion = ({ estaLogeado }) => {
  const [localPath, setLocalPath] = useState("");

  useEffect(() => {
    setLocalPath(Router.router.asPath);
  }, []);

  const classPath = (path) => {
    return `nav-link ${localPath === path ? "active" : ""}`;
  };

  return (
    <ul className="navbar-nav m-auto mb-2 mb-lg-0">
      <li className="nav-item active">
        <Link href={"/"}>
          <a className={classPath("/")}>Inicio</a>
        </Link>
      </li>
      <li className="nav-item">
        <Link href="/populares">
          <a className={classPath("/populares")}>Populares</a>
        </Link>
      </li>
      {estaLogeado && (
        <li className="nav-item">
          <Link href="/nuevo-curso">
            <a className={classPath("/nuevo-curso")}>Nuevo Curso</a>
          </Link>
        </li>
      )}
    </ul>
  );
};

export default Navegacion;
