import { useRouter } from "next/router";
import React, { useState } from "react";

const Buscador = () => {
  const [busqueda, setBusqueda] = useState("");

  const router = useRouter();

  const buscarCurso = (e) => {
    e.preventDefault();
    router.push({
      pathname: "/buscar",
      query: {
        q: busqueda,
      },
    });
  };

  return (
    <form className="d-flex" role="search" onSubmit={buscarCurso}>
      <input
        className="form-control me-2"
        type="search"
        placeholder="Buscador de cursos"
        aria-label="Search"
        onChange={(e) => setBusqueda(e.target.value)}
      />
      <button className="btn btn-outline-success" type="submit">
        Buscar
      </button>
    </form>
  );
};

export default Buscador;
