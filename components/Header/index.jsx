import React, { useState, useContext, useEffect } from "react";
import { AuthContext } from "../../context/auth";
import Botones from "./Botones";
import Buscador from "./Buscador";
import Navegacion from "./Navegacion";

const Header = () => {
  const [estaLogeado, setEstadLogeado] = useState(false);

  const { state, cerrar } = useContext(AuthContext);

  useEffect(() => {
    if (state.userInfo) {
      setEstadLogeado(true);
    } else {
      setEstadLogeado(false);
    }
  }, [state]);

  return (
    <nav className="navbar navbar-expand-lg bg-light">
      <div className="container-fluid">
        <a className="navbar-brand" href="#">
          Logo
        </a>
        <div className="collapse navbar-collapse">
          <Buscador />
          <Navegacion estaLogeado={estaLogeado} />
          <Botones
            name={state.userInfo?.displayName}
            estaLogeado={estaLogeado}
            cerrar={() => cerrar()}
          />
        </div>
      </div>
    </nav>
  );
};

export default Header;
