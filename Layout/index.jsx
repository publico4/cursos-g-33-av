import Head from "next/head";
import React from "react";
import Header from "../components/Header";

const origin = typeof window === "undefined" ? "" : window.location.origin;

const Layout = ({ children, textTitle = "Cursos Academia", urlImage }) => {
  const urlImageMeta = urlImage ? urlImage : `${origin}/academiaMoviles.png`;
  return (
    <>
      <Head>
        <title>{textTitle}</title>
        <meta name="description" content={`Información sobre ${textTitle}`} />

        <meta property="og:title" content={textTitle} />
        <meta
          property="og:description"
          content={`Informacion sobre ${textTitle}`}
        />
        <meta property="og:image" content={urlImageMeta} />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <main className="container">{children}</main>
    </>
  );
};

export default Layout;
